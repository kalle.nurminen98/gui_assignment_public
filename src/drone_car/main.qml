import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick
import QtQuick.Controls
import Qt.labs.animation 1.0
import Qt.labs.qmlmodels 1.0


Window {
    id: window
    width: 640
    height: 480
    visible: true
    title: qsTr("Drone car")
    // Colors for both stacks
    color: (stack.depth > 1) ? "white" : "lightgray"
    property int value: 0

    // Driving and flying view
    StackView {
           id: stack
           initialItem: mainView
           anchors.fill: parent
       }

       Component {
           id: mainView

           Row {
               spacing: 10

               // Button to press to get flying mode
               RoundButton {
                   id: drive
                   text: "Fly"
                   width: 40
                   height: 25
                   enabled: stack.depth < 2
                   onClicked: stack.push(mainView)
                   Text {
                       x:290; y:0
                       id: textDrive
                       font.bold: true
                       text: (stack.depth > 1) ? "" : "Driving"
                   }
               }
               // Button to press to get driving mode
               RoundButton {
                   id: fly
                   text: "Drive"
                   width: 40
                   height: 25
                   enabled: stack.depth > 1
                   onClicked: stack.pop()
                   Text {
                       x:240; y:0
                       id: textFly
                       font.bold: true
                       text: (stack.depth < 2) ? "" : "Flying"
                   }

               }
           }

       }
       // Item for image
       Item {
               id: driveContainer;
               width: 200;
               height: width;
               x: 210
               y: 120
               visible: stack.depth < 2
               property real centerX : (width / 2);
               property real centerY : (height / 2);

               // Image upload
               Image{
                   id: driveImage;
                   transformOrigin: Item.Center;
                   antialiasing: true;
                   anchors.fill: parent;
                   source: "images/steering_wheel.png"

                   // MouseArea to rotate image
                       MouseArea{
                           anchors.fill: parent;
                           onPositionChanged:  {
                               var point =  mapToItem (driveContainer, mouse.x, mouse.y);
                               var diff_x = (point.x - driveContainer.centerX);
                               var diff_y = -1 * (point.y - driveContainer.centerY);
                               var radius = Math.atan (diff_y / diff_x);
                               var degree = (radius * 180 / Math.PI);

                               if (diff_x > 0 && diff_y > 0) {
                                   driveImage.rotation = 90 - Math.abs (degree);
                               }
                               else if (diff_x > 0 && diff_y < 0) {
                                   driveImage.rotation = 90 + Math.abs (degree);
                               }
                               else if (diff_x < 0 && diff_y > 0) {
                                   driveImage.rotation = 270 + Math.abs (degree);
                               }
                               else if (diff_x < 0 && diff_y < 0) {
                                   driveImage.rotation = 270 - Math.abs (degree);
                               }

                           }
                       }
                   }

       }
       // Rectangle and text for speed value
       Rectangle {
           id: driveRect
           visible: stack.depth < 2
           x:210; y:30
           width: 200
           height: 25
           color: "transparent"
           border.color: "black"
           Text {
               anchors.fill:parent
               id: driveText
               font.pixelSize: 20
               font.bold: true
               text: value
               horizontalAlignment: Text.AlignHCenter
               verticalAlignment: Text.AlignVCenter
           }

       }
       // Button to give more speed
       RoundButton {
               visible: stack.depth < 2
               id: drivePlusButton
               x:510; y:30
               width: 60
               height: 35
               text: "+1km/h"
               background: Rectangle {
                   id: drivePlusButtonRect
                   radius: drivePlusButton.radius
                   color: "cyan"
               }
               onClicked: {
                        window.value += 1
                        drivePlusButtonAnim.running = true;
                     }


               SequentialAnimation {
                   id: drivePlusButtonAnim
                   PropertyAnimation {
                       target: drivePlusButtonRect
                       property: "color"
                       to: "green"
                       duration: 50
                   }
                   PropertyAnimation {
                       target: drivePlusButtonRect
                       property: "color"
                       to: "cyan"
                       duration: 50
                   }
               }

           }
       // Button to reduce speed
       RoundButton {
               visible: stack.depth < 2
               id: driveMinusButton
               x:580; y:30
               width: 60
               height: 35
               text: "-1km/h"
               enabled: window.value > 0
               background: Rectangle {
                   id: driveMinusButtonRect
                   radius: driveMinusButton.radius
                   color: "cyan"
               }
               onClicked: {
                        window.value -= 1
                        driveMinusButtonAnim.running = true;
                     }
               SequentialAnimation {
                   id: driveMinusButtonAnim
                   PropertyAnimation {
                       target: driveMinusButtonRect
                       property: "color"
                       to: "red"
                       duration: 50
                   }
                   PropertyAnimation {
                       target: driveMinusButtonRect
                       property: "color"
                       to: "cyan"
                       duration: 50
                   }
               }
           }
       // Button to get 100km/h in 3seconds
       RoundButton {
               visible: stack.depth < 2
               id: driveTo100Button
               x:540; y:190
               width: 100
               height: 40
               text: "To 100km/h"
               background: Rectangle {
                   id: driveTo100Rect
                   radius: driveMinusButton.radius
                   color: "green"
               }
               onClicked: {
                        driveTo100AnimColor.running = true;
                        driveTo100AnimText.running = true;
                        window.value = 100
                     }
               SequentialAnimation {
                   id: driveTo100AnimColor
                   PropertyAnimation {
                       target: driveTo100Rect
                       property: "color"
                       to: "transparent"
                       duration: 100
                   }
                   PropertyAnimation {
                       target: driveTo100Rect
                       property: "color"
                       to: "green"
                       duration: 100
                   }
               }
                   SequentialAnimation {
                       id: driveTo100AnimText
                   NumberAnimation {
                       target: driveText
                       property: "text"
                       from: window.value
                       to: 100
                       duration: 3000
               }
           }
       }
       // Button to break in 1 second to 0
       RoundButton {
               visible: stack.depth < 2
               id: driveBrakeButton
               x:540; y:240
               width: 100
               height: 40
               text: "Brakes to 0km/h"
               background: Rectangle {
                   id: driveBrakeRect
                   radius: driveMinusButton.radius
                   color: "red"
               }
               onClicked: {
                        driveBrakeAnimColor.running = true;
                        driveBrakeAnimText.running = true;
                        window.value = 0
                     }
               SequentialAnimation {
                   id: driveBrakeAnimColor
                   PropertyAnimation {
                       target: driveBrakeRect
                       property: "color"
                       to: "transparent"
                       duration: 100
                   }
                   PropertyAnimation {
                       target: driveBrakeRect
                       property: "color"
                       to: "red"
                       duration: 100
                   }
               }
                   SequentialAnimation {
                       id: driveBrakeAnimText
                   NumberAnimation {
                       target: driveText
                       property: "text"
                       from: window.value
                       to: 0
                       duration: 500
               }
               }
           }
       // Positioning for tableview
       Rectangle {
           id: driveRectView1
           visible: stack.depth < 2
           x:0; y:120
           width: 200
           height: 200
           color: "transparent"
       }
       // Tableview for list of driving features
       TableView {
               visible: stack.depth < 2
               anchors.fill: driveRectView1
               columnSpacing: 1
               rowSpacing: 1
               boundsBehavior: Flickable.StopAtBounds

               model: TableModel {

                   TableModelColumn { display: "feature";}
                   TableModelColumn { display: "on_or_off";}

                   rows: [
                       {
                           // Each property is one cell/column.
                           feature: "ESP",
                           on_or_off: "on"
                       },
                       {

                           feature: "Bluetooth",
                           on_or_off: "on"
                       },
                       {

                           feature: "Radio",
                           on_or_off: "on"
                       },
                       {

                           feature: "Heater",
                           on_or_off: "22°C"
                       },
                       {

                           feature: "Air conditioning",
                           on_or_off: "on"
                       }
                   ]
               }
               // Putting text to right place in tableview
               delegate:  Text {
                   text: model.display
                   padding: 12

                   Rectangle {
                       anchors.fill: parent
                       color: "darkorange"
                       z: -1
                   }
               }
           }
       //ProgressBar to animate battery information
       ProgressBar {
           visible: stack.depth < 2
           id: driveProgressBar
           x: 210; y:400
           width: 200
           value: 1

           NumberAnimation on value {
               id: progressBarAnim
               target: driveProgressBar
               property: "value"
               from: 1
               to: 0
               duration: 200000
       }
           MouseArea{
               id: progressBarMouse
               width: window.width
               height: window.height
               onPressed: {

                            progressBarAnim.running = true;
                   }

           }
       }
       // Text field for battery
       Rectangle {
           id: driveBatteryrect
           visible: stack.depth < 2
           x:210; y:360
           width: 200
           height: 25
           color: "transparent"
           Text {
               anchors.fill:parent
               id: driveBatteryText
               font.pixelSize: 20
               font.bold: true
               text: "Battery status"
               horizontalAlignment: Text.AlignHCenter
               verticalAlignment: Text.AlignVCenter
           }

       }

        // Item for image
       Item {
               id: flyContainer;
               width: 200;
               height: width;
               x: 210
               y: 120
               visible: stack.depth > 1
               property real centerX : (width / 2);
               property real centerY : (height / 2);

               // Image upload
               Image{
                   id: flyImage;
                   transformOrigin: Item.Center;
                   antialiasing: true;
                   anchors.fill: parent;
                   source: "images/steering_wheel.png"

                   // MouseArea to rotate image
                       MouseArea{
                           anchors.fill: parent;
                           onPositionChanged:  {
                               var point =  mapToItem (flyContainer, mouse.x, mouse.y);
                               var diff_x = (point.x - flyContainer.centerX);
                               var diff_y = -1 * (point.y - flyContainer.centerY);
                               var radius = Math.atan (diff_y / diff_x);
                               var degree = (radius * 180 / Math.PI);

                               if (diff_x > 0 && diff_y > 0) {
                                   flyImage.rotation = 90 - Math.abs (degree);
                               }
                               else if (diff_x > 0 && diff_y < 0) {
                                   flyImage.rotation = 90 + Math.abs (degree);
                               }
                               else if (diff_x < 0 && diff_y > 0) {
                                   flyImage.rotation = 270 + Math.abs (degree);
                               }
                               else if (diff_x < 0 && diff_y < 0) {
                                   flyImage.rotation = 270 - Math.abs (degree);
                               }

                           }
                       }
                   }

       }
       // Rectangle and text for speed value
       Rectangle {
           id: flyRect
           visible: stack.depth > 1
           x:210; y:30
           width: 200
           height: 25
           color: "transparent"
           border.color: "white"
           Text {
               anchors.fill:parent
               id: driveText1
               font.pixelSize: 20
               font.bold: true
               text: value
               horizontalAlignment: Text.AlignHCenter
               verticalAlignment: Text.AlignVCenter
           }

       }
       // Button to give more speed
       RoundButton {
               visible: stack.depth > 1
               id: flyPlusButton
               x:510; y:30
               width: 60
               height: 35
               text: "+1km/h"
               background: Rectangle {
                   id: flyPlusButtonRect
                   radius: flyPlusButton.radius
                   color: "cyan"
               }
               onClicked: {
                        window.value += 1
                        flyPlusButtonAnim.running = true;
                     }


               SequentialAnimation {
                   id: flyPlusButtonAnim
                   PropertyAnimation {
                       target: flyPlusButtonRect
                       property: "color"
                       to: "green"
                       duration: 50
                   }
                   PropertyAnimation {
                       target: flyPlusButtonRect
                       property: "color"
                       to: "cyan"
                       duration: 50
                   }
               }

           }
       // Button to reduce speed
       RoundButton {
               visible: stack.depth > 1
               id: flyMinusButton
               x:580; y:30
               width: 60
               height: 35
               text: "-1km/h"
               enabled: window.value > 0
               background: Rectangle {
                   id: flyMinusButtonRect
                   radius: flyMinusButton.radius
                   color: "cyan"
               }
               onClicked: {
                        window.value -= 1
                        flyMinusButtonAnim.running = true;
                     }
               SequentialAnimation {
                   id: flyMinusButtonAnim
                   PropertyAnimation {
                       target: flyMinusButtonRect
                       property: "color"
                       to: "red"
                       duration: 50
                   }
                   PropertyAnimation {
                       target: flyMinusButtonRect
                       property: "color"
                       to: "cyan"
                       duration: 50
                   }
               }
           }
       // Button to get 100km/h in 3seconds
       RoundButton {
               visible: stack.depth > 1
               id: flyTo100Button
               x:540; y:190
               width: 100
               height: 40
               text: "To 100km/h"
               background: Rectangle {
                   id: flyTo100Rect
                   radius: flyMinusButton.radius
                   color: "green"
               }
               onClicked: {
                        flyTo100AnimColor.running = true;
                        flyTo100AnimText.running = true;
                        window.value = 100
                     }
               SequentialAnimation {
                   id: flyTo100AnimColor
                   PropertyAnimation {
                       target: flyTo100Rect
                       property: "color"
                       to: "transparent"
                       duration: 100
                   }
                   PropertyAnimation {
                       target: flyTo100Rect
                       property: "color"
                       to: "green"
                       duration: 100
                   }
               }
                   SequentialAnimation {
                       id: flyTo100AnimText
                   NumberAnimation {
                       target: flyText
                       property: "text"
                       from: window.value
                       to: 100
                       duration: 3000
               }
           }
       }
       // Button to break in 1 second to 0
       RoundButton {
               visible: stack.depth > 1
               id: flyBrakeButton
               x:540; y:240
               width: 100
               height: 40
               text: "Brakes to 0km/h"
               background: Rectangle {
                   id: flyBrakeRect
                   radius: flyMinusButton.radius
                   color: "red"
               }
               onClicked: {
                        flyBrakeAnimColor.running = true;
                        flyBrakeAnimText.running = true;
                        window.value = 0
                     }
               SequentialAnimation {
                   id: flyBrakeAnimColor
                   PropertyAnimation {
                       target: flyBrakeRect
                       property: "color"
                       to: "transparent"
                       duration: 100
                   }
                   PropertyAnimation {
                       target: flyBrakeRect
                       property: "color"
                       to: "red"
                       duration: 100
                   }
               }
                   SequentialAnimation {
                       id: flyBrakeAnimText
                   NumberAnimation {
                       target: flyText
                       property: "text"
                       from: window.value
                       to: 0
                       duration: 500
               }
               }
           }
       // Positioning for tableview
       Rectangle {
           id: flyRectView1
           visible: stack.depth > 1
           x:0; y:120
           width: 200
           height: 200
           color: "transparent"
       }
       // Tableview for list of driving features
       TableView {
               visible: stack.depth > 1
               anchors.fill: flyRectView1
               columnSpacing: 1
               rowSpacing: 1
               boundsBehavior: Flickable.StopAtBounds

               model: TableModel {

                   TableModelColumn { display: "feature";}
                   TableModelColumn { display: "on_or_off";}

                   rows: [
                       {
                           // Each property is one cell/column.
                           feature: "ESP",
                           on_or_off: "on"
                       },
                       {

                           feature: "Bluetooth",
                           on_or_off: "on"
                       },
                       {

                           feature: "Radio",
                           on_or_off: "on"
                       },
                       {

                           feature: "Heater",
                           on_or_off: "22°C"
                       },
                       {

                           feature: "Air conditioning",
                           on_or_off: "on"
                       }
                   ]
               }
               // Putting text to right place in tableview
               delegate:  Text {
                   text: model.display
                   padding: 12

                   Rectangle {
                       anchors.fill: parent
                       color: "darkorange"
                       z: -1
                   }
               }
           }
       //ProgressBar to animate battery information
       ProgressBar {
           visible: stack.depth > 1
           id: flyProgressBar
           x: 210; y:400
           width: 200
           value: 1

           NumberAnimation on value {
               id: progressBarAnim1
               target: flyProgressBar
               property: "value"
               from: 1
               to: 0
               duration: 200000
       }
           MouseArea{
               id: progressBarMouse1
               width: window.width
               height: window.height
               onPressed: {

                            progressBarAnim1.running = true;
                   }

           }
       }
       // Text field for battery
       Rectangle {
           id: flyBatteryrect
           visible: stack.depth > 1
           x:210; y:360
           width: 200
           height: 25
           color: "transparent"
           Text {
               anchors.fill:parent
               id: flyBatteryText
               font.pixelSize: 20
               font.bold: true
               text: "Battery status"
               horizontalAlignment: Text.AlignHCenter
               verticalAlignment: Text.AlignVCenter
           }

       }


}

