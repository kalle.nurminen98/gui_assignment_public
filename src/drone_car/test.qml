import QtQuick 2.0
import QtTest 1.2

TestCase {
        name: "ClickTest"
        when: windowShown

        Window{
            id:window
            title: qsTr("Drone car")
        }

        function test_window_name() {

            compare(window.title, qsTr("Drone car"));
        }
    }
