# Drone car

Idea was to create a mock up of drone piloted car which can drive and fly.

# Usage

Take a git clone on project and take it to your computer.

# About the project

This was made by Kalle Nurminen and Ilona Skarp for Noroff and Experis Academy Finland.


# Responsibilities in this project

Kalle: Created driving mode and every element which id contains drive. Used tableview as model. Tested unit test and there is a PNG file for unit test results.

Ilona: Created flying mode and every element which id contains fly.

# Maintainers

[Ilona Skarp] (https://gitlab.com/iskarp)

[Kalle Nurminen] (https://gitlab.com/kalle.nurminen98)
